﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogClassLibrary
{
    public class Constants
    {
        public const decimal WetFoodPrice = 2.0m;
        public const decimal DryFoodPrice = 1.5m;
    }
}
