﻿using DogClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogFood
{
    public class Dogs
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public decimal Wetfood { get; set; }

        public decimal Dryfood { get; set; }


        public decimal perdayCost()
        {
            return Wetfood * Constants.WetFoodPrice + Dryfood * Constants.DryFoodPrice;
        }

        public decimal perweekCost()
        {
            return 7.0m * perdayCost();
        }

    }
}
