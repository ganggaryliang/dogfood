﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DogFood
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Dogs dog1, dog2, dog3, dog4;

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            //per day cost
            lblCostPerDay.Text = "Per Day Cost:" + string.Format("{0:#.00}", (dog1.perdayCost()+dog2.perdayCost()+dog3.perdayCost()+dog4.perdayCost())) +"$CAD";

            //per week cost
            lblCostPerWeek.Text = "Per Week Cost:" + string.Format("{0:#.00}", (dog1.perweekCost() + dog2.perweekCost() + dog3.perweekCost() + dog4.perweekCost())) + "$CAD";
        }

        private void btnDogFoodFill_Click(object sender, EventArgs e)
        {
            dog1 = new Dogs();
            dog1.Name = "Rex";
            dog1.Dryfood = 3m;
            dog1.Wetfood = 0m;

            dog2 = new Dogs();
            dog2.Name = "Mimi";
            dog2.Dryfood = 0m;
            dog2.Wetfood = 0.25m;

            dog3 = new Dogs();
            dog3.Name = "Fido";
            dog3.Dryfood = 0.5m;
            dog3.Wetfood = 0.5m;

            dog4 = new Dogs();
            dog4.Name = "Sugar";
            dog4.Dryfood = 0.5m;
            dog4.Wetfood = 0.25m;

        }
    }
}
