﻿namespace DogFood
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDogFoodFill = new System.Windows.Forms.Button();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lblCostPerDay = new System.Windows.Forms.Label();
            this.lblCostPerWeek = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDogFoodFill
            // 
            this.btnDogFoodFill.Location = new System.Drawing.Point(155, 119);
            this.btnDogFoodFill.Name = "btnDogFoodFill";
            this.btnDogFoodFill.Size = new System.Drawing.Size(75, 23);
            this.btnDogFoodFill.TabIndex = 0;
            this.btnDogFoodFill.Text = "DogFoodFill";
            this.btnDogFoodFill.UseVisualStyleBackColor = true;
            this.btnDogFoodFill.Click += new System.EventHandler(this.btnDogFoodFill_Click);
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(357, 118);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 1;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lblCostPerDay
            // 
            this.lblCostPerDay.AutoSize = true;
            this.lblCostPerDay.Location = new System.Drawing.Point(357, 201);
            this.lblCostPerDay.Name = "lblCostPerDay";
            this.lblCostPerDay.Size = new System.Drawing.Size(69, 13);
            this.lblCostPerDay.TabIndex = 2;
            this.lblCostPerDay.Text = "Cost Per Day";
            // 
            // lblCostPerWeek
            // 
            this.lblCostPerWeek.AutoSize = true;
            this.lblCostPerWeek.Location = new System.Drawing.Point(360, 246);
            this.lblCostPerWeek.Name = "lblCostPerWeek";
            this.lblCostPerWeek.Size = new System.Drawing.Size(79, 13);
            this.lblCostPerWeek.TabIndex = 3;
            this.lblCostPerWeek.Text = "Cost Per Week";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblCostPerWeek);
            this.Controls.Add(this.lblCostPerDay);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.btnDogFoodFill);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDogFoodFill;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Label lblCostPerDay;
        private System.Windows.Forms.Label lblCostPerWeek;
    }
}

